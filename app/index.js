'use strict';
(function(){
	require('angular');

	var exService = require('./services/exchange.js');
	var mainCtrl = require('./controllers/mainCtrl.js');

	var ng = angular.module('app', []);
	
    ng.factory('exService', ['$http',  function($http){
            return new exService($http);
    }] );

    ng.controller('mainCtrl', ['$scope', 'exService', mainCtrl ]);

    angular.element(document).ready(function() {
        angular.bootstrap(document, [ng.name]);
    }); 

})();




//browserify app/index.js -o app/compile.js -d