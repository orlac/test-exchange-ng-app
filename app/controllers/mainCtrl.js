'use strict';
var _ = require('lodash');
module.exports = function($scope, exService){

	$scope.currency = []
	$scope.from=null;
	$scope.to=null;
	$scope.num=null;
	$scope.error=null;
	$scope.result=null;
	$scope.rate=null;

	$scope.getToArr = function(){
		var _arr = _.filter($scope.currency, function(cur){ 
			return cur !== $scope.from; 
		});
		return _arr;
	}

	$scope.exchange = function(){
		$scope.error=null;
		$scope.result=null;
		$scope.rate=null;
		if($scope.hasCanExchange()){
			exService.getExchange($scope.from, $scope.to, $scope.num, function(data){
				$scope.result = data.v;
				$scope.rate = data.rate;
				$scope.$apply();
			},function(err){
				$scope.error = err || 'Какая-то непонятная ошибка :(';
				$scope.$apply();
			})
		}
	}

	$scope.hasCanExchange = function(){
		return ($scope.from && $scope.to);
	}	

	exService.getCurrency(function(data){
		$scope.currency = data;
	});




}