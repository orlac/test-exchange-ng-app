'use strict';

module.exports = function($http){
    
    var s = {};

    var cur = [
        'EUR'
        ,'RUB'
    ];

    s.getCurrency=function(cb){
        cb(cur);
    }

    s.getExchange=function(from, to, num, cb, onError){
        
        $http.jsonp('http://rate-exchange.appspot.com/currency', 
        {
            params: {
                from: from,
                to: to,
                q: num,
                r: Math.random(),
                callback: 'JSON_CALLBACK' 
            }
        })
        .success(function(data, status, headers, config){
            if(data.warning){
                onError(data.warning);    
            }else{
                cb(data);    
            }
        })
        .error(function(data, status, headers, config){
            onError(status);
        });

    };

    
    return s;
}; 
